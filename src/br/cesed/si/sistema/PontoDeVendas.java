package br.cesed.si.sistema;

import java.util.Scanner;

import br.cesed.si.sistema.carrinho.Carrinho;
import br.cesed.si.sistema.carrinho.ProdutosComprados;
import br.cesed.si.sistema.estoque.Estoque;

public class PontoDeVendas {
	public static void main(String[] args) {
		Estoque estoque = new Estoque();
		Carrinho carrinho = new Carrinho();
		ProdutosComprados comprados = new ProdutosComprados();

		System.out.println(":: Sistema Assai de Vendas - Powered by SI ::\n");
		System.out.println("Op��es:");
		System.out.println(
				"1) Adicionar um produto espec�fico ao carrinho\n" 
				+ "2) Encerrar a compra\n" + "3) Sair do sistema\n");
		Scanner entrada = new Scanner(System.in);
		int opcao = entrada.nextInt();
		

		switch (opcao) {
		case 1:
			estoque.produtosDisponiveis();
			System.out.println("\nDigite o c�digo do produto escolhido (ou 0 para sair deste menu)");
			
			break;

		case 2:
			System.out.println(":: Listagem de Produtos::");
			comprados.listaDeCompras();
			System.out.println("Total: R$");
			System.out.println("Digite 1 para encerrar a compra ou 2 para voltar ao menu inicial");
			
			int seletor = entrada.nextInt();
		
			switch (seletor) {
			case 1:
				System.out.println("Aguarde...");
				break;

			case 2:
				System.out.println("Voltando para o menu");
				break;
			}
			break;
			
		case 3:
			System.out.println("Sistema encerrado, volte sempre :) ");
			break;
		}
		
		entrada.close();
	}
}
