package br.cesed.si.sistema.carrinho;

import br.cesed.si.sistema.produto.Produto;

public class Carrinho {
	private Produto inicio;
	private ProdutosComprados produtosComprados = new ProdutosComprados();
	
	public void comprar(Produto produto) {
		if(inicio == null) {
			inicio = produto;
			
		} else {
			Produto temp = inicio;
			
			while(temp.getProximo() != null) {
				temp = temp.getProximo();
			}
			
			temp.setProximo(produto);
		}
		
		produtosComprados.add(produto);
		produtosComprados.listaDeCompras();
	}
}
