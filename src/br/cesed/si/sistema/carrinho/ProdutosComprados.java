package br.cesed.si.sistema.carrinho;

import br.cesed.si.sistema.estoque.Estoque;
import br.cesed.si.sistema.produto.Produto;

public class ProdutosComprados {
	Produto[] comprados= new Produto[Estoque.TAMANHO_INICIAL];
	private int adicionados;
	
	public void add(Produto produto) {
		if(adicionados == comprados.length) {
			Produto[] novoArray = new Produto[comprados.length + Estoque.TAMANHO_INICIAL];
			for(int i = 0; i < comprados.length; i++) {
				novoArray[i] = comprados[i];
			}
			
			comprados = novoArray;
		}
		
		comprados[adicionados] = produto;
		adicionados++;
	}
	
	public void listaDeCompras() {
		for(int i = 0; i < adicionados; i++) {
			System.out.println(comprados[i].getNome() + " - Quantidade " + comprados[i].getQuantidadeEmEstoque() 
					+ " - Pre�o Unit�rio " + "R$" + comprados[i].getPreco());
		}
	}
}
