package br.cesed.si.sistema.estoque;

import br.cesed.si.sistema.produto.Produto;

public class Estoque {
	public static final int TAMANHO_INICIAL = 3;
	public Produto[] estoque = new Produto[TAMANHO_INICIAL];
	private int quantidade;
	
	public Estoque() {
		super();
		this.add(new Produto("Bolacha Oreo", 2.5, 100));
		this.add(new Produto("Bolacha Bono", 1.7, 80));
	}

	public void add(Produto produto) {
		if(quantidade == estoque.length) {
			Produto[] novoEstoque = new Produto[estoque.length + TAMANHO_INICIAL];
			for(int i = 0; i < estoque.length; i++) {
				novoEstoque[i] = estoque[i];
			}
			
			estoque = novoEstoque;
		}
		
		estoque[quantidade] = produto;
		quantidade++;
	}
	
	public void produtosDisponiveis() {
		for (int i = 0; i < quantidade; i++) {
			System.out.println( estoque[i].getNome() + " - Quantidade " + estoque[i].getQuantidadeEmEstoque() 
					+ " - Pre�o Unit�rio " + "R$" + estoque[i].getPreco()); 
		}
	}
}
