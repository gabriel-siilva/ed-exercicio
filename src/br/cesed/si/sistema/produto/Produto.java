package br.cesed.si.sistema.produto;

public class Produto {
	private Produto proximo;
	private String nome;
	private double preco;
	private int quantidadeEmEstoque;
	
	public Produto(String nome, double preco, int quantidadeEmEstoque) {
		super();
		this.nome = nome;
		this.preco = preco;
		this.quantidadeEmEstoque = quantidadeEmEstoque;
	}

	public Produto getProximo() {
		return proximo;
	}

	public void setProximo(Produto proximo) {
		this.proximo = proximo;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public double getPreco() {
		return preco;
	}

	public void setPreco(double preco) {
		this.preco = preco;
	}

	public int getQuantidadeEmEstoque() {
		return quantidadeEmEstoque;
	}

	public void setQuantidadeEmEstoque(int quantidadeEmEstoque) {
		this.quantidadeEmEstoque = quantidadeEmEstoque;
	}	
}
